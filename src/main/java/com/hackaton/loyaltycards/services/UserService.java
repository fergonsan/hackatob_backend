package com.hackaton.loyaltycards.services;

import com.hackaton.loyaltycards.models.CompanyModel;
import com.hackaton.loyaltycards.models.UserModel;
import com.hackaton.loyaltycards.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(){
        return this.userRepository.findAll();
    }
    public UserServiceResponse findById (String dni){
        UserServiceResponse result = new UserServiceResponse();
        if (this.userRepository.findById(dni).isPresent()){
            result.setMsg("Usuario encontrado");
            result.setUser(this.userRepository.findById(dni).get());
            result.setHttpStatus(HttpStatus.OK);
            return result;
        }
        result.setMsg("Usuario no encontrado");
        result.setUser(null);
        result.setHttpStatus(HttpStatus.NOT_FOUND);
        return result;
    }
    public UserServiceResponse add (UserModel user){
        UserServiceResponse result = new UserServiceResponse();
        if (!this.userRepository.findById(user.getDni()).isPresent()) {
            if (user.getDni().equals("") || user.getNombre().equals("")) {
                result.setMsg("El DNI y el nombre son campos obligatorios.");
                result.setUser(user);
                result.setHttpStatus(HttpStatus.BAD_REQUEST);
                return result;
            }
            this.userRepository.save(user);
            result.setMsg("Usuario creado correctamente");
            result.setUser(user);
            result.setHttpStatus(HttpStatus.CREATED);
            return result;
        }
        result.setMsg("Usuario ya existente");
        result.setUser(user);
        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        return result;
    }
    public UserServiceResponse delete (String dni){
        UserServiceResponse result = new UserServiceResponse();
        Optional<UserModel> user = this.userRepository.findById(dni);
        if (user.isPresent()){
            this.userRepository.delete(user.get());
            result.setMsg("Usuario borrado correctamente");
            result.setUser(user.get());
            result.setHttpStatus(HttpStatus.OK);
            return result;
        }
        result.setMsg("Usuario no encontrado");
        result.setUser(null);
        result.setHttpStatus(HttpStatus.BAD_REQUEST);
        return result;
    }
    public UserServiceResponse update (UserModel user){
        UserServiceResponse result = new UserServiceResponse();
        Optional<UserModel> DBUser = this.userRepository.findById(user.getDni());
        if (DBUser.isPresent()){
            this.userRepository.save(user);
            result.setHttpStatus(HttpStatus.OK);
            result.setMsg("Usuario modificado correctamente.");
            result.setUser(user);
            return result;
        }
        result.setHttpStatus(HttpStatus.NOT_FOUND);
        result.setMsg("Usuario no encontrado.");
        result.setUser(user);
        return result;
    }
    public UserServiceResponse partialUpdate (UserModel user){
        UserServiceResponse result = new UserServiceResponse();
        Optional<UserModel> DBUser = this.userRepository.findById(user.getDni());
        if (DBUser.isPresent()){
            UserModel userUp = DBUser.get();
            if(user.getNombre()!=null){
                userUp.setNombre(user.getNombre());
            }
            if(user.getDireccion()!=null){
                userUp.setDireccion(user.getDireccion());
            }
            if(user.getTelefono()!=null){
                userUp.setTelefono(user.getTelefono());
            }
            this.userRepository.save(userUp);
            result.setHttpStatus(HttpStatus.OK);
            result.setMsg("Usuario modificado correctamente.");
            result.setUser(user);
            return result;
        }
        result.setHttpStatus(HttpStatus.NOT_FOUND);
        result.setMsg("Usuario no encontrado.");
        result.setUser(user);
        return result;
    }

}
