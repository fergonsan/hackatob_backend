package com.hackaton.loyaltycards.models;

import com.mongodb.lang.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection="Cards")
public class CardModel {

    @Id
    @NonNull
    private String pan;
    @NonNull
    private String dni;
    @NonNull
    private String cif;
    private int cantidad;
    private List<MovementModel> movs;
    public CardModel() {
    }
    public CardModel(@NonNull String pan, @NonNull String dni, @NonNull String cif) {
        this.pan = pan;
        this.dni = dni;
        this.cif = cif;
    }
    @NonNull
    public String getPan() {
        return pan;
    }
    public void setPan(@NonNull String pan) {
        this.pan = pan;
    }
    @NonNull
    public String getDni() {
        return dni;
    }
    public void setDni(@NonNull String dni) {
        this.dni = dni;
    }
    @NonNull
    public String getCif() {
        return cif;
    }
    public void setCif(@NonNull String cif) {
        this.cif = cif;
    }
    public int getCantidad() {
        return cantidad;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public List<MovementModel> getMovs() {
        return movs;
    }
    public void setMovs(List<MovementModel> movs) {
        this.movs = movs;
    }
}
