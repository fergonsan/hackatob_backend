package com.hackaton.loyaltycards.repositories;

import com.hackaton.loyaltycards.models.CardModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends MongoRepository<CardModel, String> {
}
