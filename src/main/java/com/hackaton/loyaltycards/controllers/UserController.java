package com.hackaton.loyaltycards.controllers;

import com.hackaton.loyaltycards.models.UserModel;
import com.hackaton.loyaltycards.services.UserService;
import com.hackaton.loyaltycards.services.UserServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/loyalty/v1/user")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("")
    public ResponseEntity<List<UserModel>> getUsers(){
        System.out.println("getUsers");
        return new ResponseEntity<>(this.userService.findAll(), HttpStatus.OK);
    }
    @GetMapping("/{dni}")
    public ResponseEntity<UserServiceResponse>getUserById(@PathVariable String dni){
        System.out.println("getUserById");
        System.out.println("El dni es: "+dni);
        UserServiceResponse result = this.userService.findById(dni);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @DeleteMapping("/{dni}")
    public ResponseEntity<UserServiceResponse>delete(@PathVariable String dni){
        System.out.println("deleteUser");
        System.out.println("La id es: "+dni);
        UserServiceResponse result = this.userService.delete(dni);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @PostMapping("")
    public ResponseEntity<UserServiceResponse>addUser(@RequestBody UserModel newUser){
        System.out.println("addUser");
        System.out.println("El dni del usuario es: "+newUser.getDni());
        System.out.println("El nombre del usuario es: "+newUser.getNombre());
        System.out.println("El telefono del usuario es: "+newUser.getTelefono());
        System.out.println("La direccion del usuario es: "+newUser.getDireccion());
        UserServiceResponse result = this.userService.add(newUser);
        return new ResponseEntity<>(result, result.getHttpStatus());
    }
    @PatchMapping("")
    public ResponseEntity<UserServiceResponse> patchUser (@RequestBody UserModel user){
        System.out.println("patchUser");
        System.out.println("El dni del usuario es: "+user.getDni());
        System.out.println("El nombre del usuario es: "+user.getNombre());
        System.out.println("El telefono del usuario es: "+user.getTelefono());
        System.out.println("La direccion del usuario es: "+user.getDireccion());
        UserServiceResponse result = this.userService.partialUpdate(user);
        return new ResponseEntity<>(result, result.getHttpStatus());

    }

}
